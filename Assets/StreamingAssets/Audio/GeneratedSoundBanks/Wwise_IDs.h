/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ALL_OFF = 2801173556U;
        static const AkUniqueID AUTUMN_OFF = 1341869933U;
        static const AkUniqueID AUTUMN_ON = 2681980833U;
        static const AkUniqueID PAUSE = 3092587493U;
        static const AkUniqueID PLAY = 1256202815U;
        static const AkUniqueID PLAY_SEASONS_LP = 1140706515U;
        static const AkUniqueID PLAY_TEST = 3187507146U;
        static const AkUniqueID SUMMER_OFF = 1028965400U;
        static const AkUniqueID SUMMER_ON = 2035052610U;
        static const AkUniqueID WINTER_OFF = 2499407148U;
        static const AkUniqueID WINTER_ON = 2171753414U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AUTUMN
        {
            static const AkUniqueID GROUP = 4010875483U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace AUTUMN

        namespace SPRING
        {
            static const AkUniqueID GROUP = 1296465106U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace SPRING

        namespace SUMMER
        {
            static const AkUniqueID GROUP = 3924562482U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace SUMMER

        namespace WINTER
        {
            static const AkUniqueID GROUP = 2965343494U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace WINTER

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID BANK_ITISNOW = 2302513619U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AUTUMN = 4010875483U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID SEASONS = 2823114633U;
        static const AkUniqueID SPRING = 1296465106U;
        static const AkUniqueID SUMMER = 3924562482U;
        static const AkUniqueID WINTER = 2965343494U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
