using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeManager : MonoBehaviour
{
    [SerializeField] private float fadeInTime;
    [SerializeField] private float fadeOutTime;

    public float FadeInTime {   get{ return fadeInTime; }
                                private set{ fadeInTime = value; }
                            }

    public float FadeOutTime{   get{ return fadeOutTime;}
                                private set{ fadeOutTime = value; }
                            }
}
