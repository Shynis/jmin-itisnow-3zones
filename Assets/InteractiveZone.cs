using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InteractiveZone : MonoBehaviour
{
    [SerializeField] private Image ActivatedState;
    [SerializeField] private ParticleSystem particleSys;

    [SerializeField] private UnityEvent OnZoneActivated;
    [SerializeField] private UnityEvent OnZoneDeactivated;

    private BoxCollider2D zoneCollider;
    private RectTransform rectTransform;

    private bool isActivated;

    private ParticleSystemRenderer particleRenderer;

    // private HeadLocationManager headLocationManager;
    private FadeManager fadeManager;
    private IEnumerator currentImageCoroutine;
    private IEnumerator currentParticleCoroutine;

    private void Start()
    {
        // headLocationManager = FindObjectOfType<HeadLocationManager>();
        fadeManager = FindObjectOfType<FadeManager>();

        rectTransform = GetComponent<RectTransform>();
        zoneCollider = GetComponent<BoxCollider2D>();

        particleRenderer = particleSys.GetComponent<ParticleSystemRenderer>();

        SetColliderSize();
        Initialize();
    }

    private void Initialize()
    {
        isActivated = false;
        SetAlpha(0);

        // Disable emission of the particle system
        var em = particleSys.emission;
        em.enabled = false;
    }

    public void ActivateZone()
    {
        // SetAlpha(1f);

        if(!isActivated)
        {
            // Debug.Log("Activate " + this.name);

            // Images

            if(currentImageCoroutine != null)
                StopCoroutine(currentImageCoroutine);

            FadeInImage(ActivatedState);

            // SetAlpha(0);

            // Particles

            // Enable emission of the particle system
            var em = particleSys.emission;
            em.enabled = true;

            if(currentParticleCoroutine != null)
                StopCoroutine(currentParticleCoroutine);
        }

        isActivated = true;
    }

    public void DeactivateZone()
    {
        // SetAlpha(0f);

        if(isActivated)
        {
            // Debug.Log("Deactivate " + this.name);

            // Image
            if(currentImageCoroutine != null)
                StopCoroutine(currentImageCoroutine);

            FadeOutImage(ActivatedState);

            // Particles

            // Disable emission of the particle system
            var em = particleSys.emission;
            em.enabled = false;

            if(currentParticleCoroutine != null)
                StopCoroutine(currentParticleCoroutine);
        }

        isActivated = false;
    }

    private void SetColliderSize()
    {
        zoneCollider.size = rectTransform.rect.size;
    }

    private void SetAlpha(float targetAlpha)
    {
        Color tempColor = ActivatedState.color;
        tempColor.a = targetAlpha;
        ActivatedState.color = tempColor;
    }

    private void FadeInImage(Image target)
    {
        OnZoneActivated.Invoke();
        currentImageCoroutine = FadeImage(target, 1f, fadeManager.FadeInTime);
        StartCoroutine(currentImageCoroutine);
    }

    private void FadeOutImage(Image target)
    {
        // Action onCoroutineEnd = () => { isActivated = false; };
        OnZoneDeactivated.Invoke();
        currentImageCoroutine = FadeImage(target, 0f, fadeManager.FadeOutTime);
        StartCoroutine(currentImageCoroutine);

        // currentParticleCoroutine = FadeParticle(0, fadeManager.FadeOutTime);
        // StartCoroutine(currentParticleCoroutine);
    }

    // Fade coroutine
    private IEnumerator FadeImage(Image target, float targetAlpha, float fadeTime, Action OnCoroutineEnd = null)
    {
        float elapsedTime = 0f;

        Color tempColor = target.color;
        float initialAlpha = tempColor.a;

        while(elapsedTime < fadeTime)
        {
            tempColor.a = Mathf.Lerp(initialAlpha, targetAlpha, elapsedTime);
            target.color = tempColor;

            yield return null;

            elapsedTime += Time.deltaTime;
        }

        tempColor.a = targetAlpha;
        target.color = tempColor;

        if(OnCoroutineEnd != null)
        {
            OnCoroutineEnd();
        }

        // Debug.Log("End coroutine " + this.name + " fade to " + targetAlpha);
    }

    private IEnumerator FadeParticle(float targetAlpha, float fadeTime)
    {
        Debug.Log("Start particle coroutine");

        float elapsedTime = 0f;

        // Color tempColor = particleRenderer.material.color;
        Color tempColor = particleRenderer.material.GetColor("_TintColor");
        float initialAlpha = tempColor.a;

        while(elapsedTime < fadeTime)
        {
            tempColor.a = Mathf.Lerp(initialAlpha, targetAlpha, elapsedTime);
            // particleRenderer.material.color = tempColor;
            particleRenderer.material.SetColor("_TintColor", tempColor);

            yield return null;

            elapsedTime += Time.deltaTime;
        }

        tempColor.a = targetAlpha;
        particleRenderer.material.color = tempColor;

        Debug.Log("End particle coroutine");
    }
}
