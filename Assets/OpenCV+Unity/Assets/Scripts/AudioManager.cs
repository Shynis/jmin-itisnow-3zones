using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioManager : MonoBehaviour
{
    //---------------------------------------- Debug ----------------------------------------//
    public void Test_Event()
    {
        AkSoundEngine.PostEvent("play_TEST", gameObject);
    }
    public void Resume()
    {
        AkSoundEngine.PostEvent("Play", gameObject);
    }
    public void Play()
    {
        AkSoundEngine.PostEvent("Pause", gameObject);
    }
    public void all_states_off()
    {
        AkSoundEngine.PostEvent("all_off", gameObject);
    }

    //---------------------------------------- Gameplay ----------------------------------------//

    public void Start_Seasons_lp()
    {
        AkSoundEngine.PostEvent("Play_Seasons_lp", gameObject);
    }
    public void spring_on()
    {
        AkSoundEngine.PostEvent("summer_on", gameObject); // trompé de nomenclature dans le Wwise, il faut bien inverser ici Summer et Spring
    }
    public void spring_off()
    {
        AkSoundEngine.PostEvent("summer_off", gameObject); // trompé de nomenclature dans le Wwise, il faut bien inverser ici Summer et Spring
    }

    public void winter_on()
    {
        AkSoundEngine.PostEvent("winter_on", gameObject); 
    }
    public void winter_off()
    {
        AkSoundEngine.PostEvent("winter_off", gameObject);
    }

    public void autumn_on()
    {
        AkSoundEngine.PostEvent("autumn_on", gameObject);
    }
    public void autumn_off()
    {
        AkSoundEngine.PostEvent("autumn_off", gameObject);
    }

}
